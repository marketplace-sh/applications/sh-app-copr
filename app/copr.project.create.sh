#!/usr/bin/env bash

# -------------------------------------------------------------------------------------------------------------------- #
# Fedora Copr API. Create project.
# -------------------------------------------------------------------------------------------------------------------- #
# @author Kitsune Solar <kitsune.solar@gmail.com>
# @version 1.0.0
# -------------------------------------------------------------------------------------------------------------------- #

token="${1}"
project_name="${2}"; IFS=';' read -a project_name <<< "${project_name}"

curl="$( which curl )"
api_ver="2"
sleep="2"

if (( ! ${#project_name[@]} )); then exit 1; fi

for i in "${project_name[@]}"; do
    echo ""
    echo "--- Open: ${i}"

    ${curl}                                     \
    --header "Host: copr.fedoraproject.org"     \
    --header "Authorization: Basic ${token}"    \
    --header "Content-Type: application/json"   \
    --request POST                              \
    --data "{\"disable_createrepo\": false, \"build_enable_net\": false, \"name\": \"${i}\", \"chroots\": [\"fedora-29-x86_64\"]}" \
    "/api_${api_ver}/projects"

    echo ""
    echo "--- Done: ${i}"
    echo ""

    sleep ${sleep}
done

exit 0
