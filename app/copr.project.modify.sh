#!/usr/bin/env bash

# -------------------------------------------------------------------------------------------------------------------- #
# Fedora Copr API. Modify project.
# -------------------------------------------------------------------------------------------------------------------- #
# @author Kitsune Solar <kitsune.solar@gmail.com>
# @version 1.0.0
# -------------------------------------------------------------------------------------------------------------------- #

# -------------------------------------------------------------------------------------------------------------------- #
# Get options.
# -------------------------------------------------------------------------------------------------------------------- #

OPTIND=1

while getopts "d:p:h" opt; do
    case ${opt} in
        d)
            distr_name="${OPTARG}"
            ;;
        p)
            package_name="${OPTARG}"
            ;;
        h|*)
            echo "-d [distr_name] -p [package_name]"
            exit 2
            ;;
        \?)
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
    esac
done

shift $(( ${OPTIND} - 1 ))

if [[ -z "${distr_name}" ]] || [[ -z "${package_name}" ]]; then exit 1; fi

# -------------------------------------------------------------------------------------------------------------------- #
# Set description.
# -------------------------------------------------------------------------------------------------------------------- #

url_copr="https://copr.fedorainfracloud.org/coprs/marketplace"

read -r -d '' set_desc << EOF
- Package: [${package_name}](${url_copr}/${distr_name}-${package_name}/packages/)
- Maintainer: [METADATA Foundation](https://metadata.foundation/)

#### Links

- [Support](https://unix.community/)
- [Documentation](https://sysadmins.wiki/)
- [Source](https://gitlab.com/marketplace-${distr_name}/packages/${distr_name}-srpm-${package_name})
- [Issues](https://gitlab.com/marketplace-${distr_name}/packages/${distr_name}-srpm-${package_name}/issues)

#### Donation

- [Liberapay](https://liberapay.com/metadata/donate)
- [Patreon](https://patreon.com/metadata)
EOF

# -------------------------------------------------------------------------------------------------------------------- #
# Set instructions.
# -------------------------------------------------------------------------------------------------------------------- #

read -r -d '' set_inst << EOF
1. Enable repository **MARKETPLACE**: \`dnf copr enable marketplace/${distr_name}-${package_name}\`.
2. Install package: \`dnf install ${package_name}\`.
EOF

# -------------------------------------------------------------------------------------------------------------------- #
# Copr CLI.
# -------------------------------------------------------------------------------------------------------------------- #

copr-cli modify --description "${set_desc}" --instructions "${set_inst}" ${distr_name}-${package_name}

# -------------------------------------------------------------------------------------------------------------------- #
# Exit.
# -------------------------------------------------------------------------------------------------------------------- #

exit 0
